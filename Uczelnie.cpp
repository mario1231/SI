#define _CRT_SECURE_NO_WARNINGS

#include <fstream>
#include <iostream>
#include <algorithm>
#include "Uczelnie.h"

string Uczelnie::uczelniePlik = "pliki/uczelnie.txt";

Uczelnie::Uczelnie() {
	wczytajUczelnie();
}

vector<Uczelnia> Uczelnie::getUczelnie() {
	return uczelnie;
}

void Uczelnie::wczytajUczelnie() {
	ifstream fOut;
	fOut.open(Uczelnie::uczelniePlik, ios::in);
	string buffer;
	
	if (fOut.good()) {
		while (!fOut.eof()) {
			char* m;
			char* bufferChar;
			Uczelnia uczelnia;

			//Wczytaj nazw�
			buffer.clear();
			getline(fOut, buffer);
			uczelnia.nazwa = buffer;
		
			//Wczytaj numer w rankingu
			buffer.clear();
			getline(fOut, buffer);
			uczelnia.ranking = stoi(buffer);

			//Wczytaj miasto
			buffer.clear();
			getline(fOut, buffer);
			uczelnia.miasto = buffer;

			//Wczytaj tytu�y
			buffer.clear();
			getline(fOut, buffer);

			bufferChar = new char[buffer.length() + 1];
			strcpy(bufferChar, buffer.c_str());
			m = strtok(bufferChar, ",");

			while (m != NULL) {
				uczelnia.tytulyNaukowe.push_back(string(m));
				m = strtok(NULL, ",");
			}

			delete[] bufferChar;

			//Wczytaj tryby studi�w
			buffer.clear();
			getline(fOut, buffer);

			bufferChar = new char[buffer.length() + 1];
			strcpy(bufferChar, buffer.c_str());
			m = strtok(bufferChar, ",");

			while (m != NULL) {
				uczelnia.trybyStudiow.push_back(string(m));
				m = strtok(NULL, ",");
			}

			delete[] bufferChar;

			//Wczytaj kierunki
			buffer.clear();
			getline(fOut, buffer);

			bufferChar = new char[buffer.length() + 1];
			strcpy(bufferChar, buffer.c_str());
			m = strtok(bufferChar, ",");

			while (m != NULL) {
				uczelnia.kierunki.push_back(string(m));
				m = strtok(NULL, ",");
			}

			delete [] bufferChar;
			uczelnie.push_back(uczelnia);
		}

		fOut.close();

		sortujPoRankingu();
	}
	else
		cout << "Blad otwarcia pliku " << Uczelnie::uczelniePlik << ".txt\n";
}

void Uczelnie::sortujPoRankingu() {
	sort(uczelnie.begin(), uczelnie.end(), [](Uczelnia& a, Uczelnia&b) -> bool {
		return a.ranking < b.ranking; 
	});
}