#include <algorithm>
#include <numeric>
#include "Poszukiwacz.h"

Poszukiwacz::Poszukiwacz(Uczelnie& u, Odpowiedzi& o) : 
	uczelnie(u), odpowiedzi(o)
{
}

void Poszukiwacz::wyszukaj() {
	vector<Uczelnia> wyszukane(uczelnie.getUczelnie());
	vector<Uczelnia> tmp;

	//Eliminacja uczelni, kt�re nie posiadaj� odpowiedniego kierunku
	for (Uczelnia u : wyszukane)
		for (string s : u.kierunki)
			if (s == odpowiedzi.getKierunek()) {
				tmp.push_back(u);
				break;
			}

	wyszukane = vector<Uczelnia>(tmp);
	tmp.clear();

	//Eliminacja uczelni, kt�re nie posiadaj� odpowiedniego trybu
	for (Uczelnia u : wyszukane)
		for (string s : u.trybyStudiow)
			if (s == odpowiedzi.getTrybStudiow()) {
				tmp.push_back(u);
				break;
			}

	wyszukane = vector<Uczelnia>(tmp);
	tmp.clear();

	//Eliminacja uczelni, kt�re nie posiadaj� odpowiedniego tytu�u naukowego
	for (Uczelnia u : wyszukane)
		for (string s : u.tytulyNaukowe)
			if (s == odpowiedzi.getTytulNaukowy()) {
				tmp.push_back(u);
				break;
			}

	wyszukane = vector<Uczelnia>(tmp);
	tmp.clear();

	//Eliminacja uczelni, kt�re nie s� z podanego miasta
	for (Uczelnia u : wyszukane)
		if (u.miasto == odpowiedzi.getMiasto()) {
			tmp.push_back(u);
			break;
		}

	wyszukane = vector<Uczelnia>(tmp);
	tmp.clear();

	//Teraz liczymy �redni� z matury
	//Im wi�ksza �rednia, tym wy�ej w rankingu uczelni� wybierze system
	double srednia;
	//Srednia z matur podstawowych (3)
	srednia = accumulate(odpowiedzi.getWynikiZMatury().begin(), odpowiedzi.getWynikiZMatury().end(), 0.0);

	//Srednia z podstawowych + rozszerzenie
	if (odpowiedzi.getIleRozszerzen() != 0) {
		srednia += 2 * accumulate(odpowiedzi.getWynikRozszerzen().begin(), odpowiedzi.getWynikRozszerzen().end(), 0.0) / odpowiedzi.getIleRozszerzen();
		cout << accumulate(odpowiedzi.getWynikRozszerzen().begin(), odpowiedzi.getWynikRozszerzen().end(), 0.0) / odpowiedzi.getIleRozszerzen();
		srednia /= 5;
	}
	else
		srednia /= 3;

	//Je�li jest olimpiada - tacy maj� pierwsze�stwo w rekrutacji, wi�c dajemy im 100%
	if (odpowiedzi.getCzyOlimpiada())
		srednia = 100.0;

	//Wy�wietlamy znalezion� uczelni�
	if (wyszukane.size() > 0) {
		//Na podstawie �redniej wybieramy uczelni� z odpowiednim miejscem w rankingu:
		int ranking = (int)floor(srednia / 100 * (wyszukane.size()-1));

		cout << "\n\nOto znaleziona uczelnia:\n\n";
		cout << wyszukane.at(wyszukane.size() - 1 - ranking).nazwa << endl;
	}
	else {
		cout << "\n\nNiestety, nie znaleziono uczelni spelniajacej Twoje preferencje.";
	}
}