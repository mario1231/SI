#pragma once

#include <string>
#include <iostream>
#include <vector>

using namespace std;

/*
	Struktura reprezentująca uczelnię.
	Zawiera wczytane z pliku tekstowego parametry uczelni.
*/
struct Uczelnia {
	string nazwa;
	int ranking;
	string miasto;
	vector<string> tytulyNaukowe;
	vector<string> trybyStudiow;
	vector<string> kierunki;

	/*
		Funkcja wypisująca parametry uczelni na standardowe wyjście.
	*/
	void wypisz() {
		cout << "Nazwa: " << nazwa << endl;
		cout << "Numer w rankingu: " << ranking << endl;
		cout << "Miasto: " << miasto << endl;

		cout << "Tytuly: ";
		for (string s : tytulyNaukowe)
			cout << s << ", ";
		cout << endl;

		cout << "Tryby: ";
		for (string s : trybyStudiow)
			cout << s << ", ";
		cout << endl;

		cout << "Kierunki: ";
		for (string s : kierunki)
			cout << s << ", ";
		cout << endl;
	}
};