#pragma once

#include <vector>
#include <string>

using namespace std;

/*
	Klasa Odpowiedzi

	Przechowuje mo�liwe odpowiedzi oraz odpowiedzi udzielone przez u�ytkownika.
	Mo�liwe odpowiedzi wczytuje z plik�w tekstowych.
	Za jej pomoc� zadajemy pytania u�ytkownikowi.
*/
class Odpowiedzi {
private:
	//Mo�liwe odpowiedzi wczytywane z pliku
	vector<string> zainteresowania;
	vector<string> kierunki;
	vector<string> trybyStudiow;
	vector<string> tytulyNaukowe;
	vector<string> wojewodztwa;
	vector<pair<string, vector<string>>> miasta;
	vector<string> przedmiotyMaturalne;

	//�cie�ka do plik�w
	static string folderPlikow;

	//Nazwy plik�w z odpowiedziami
	static string zainteresowaniaPlik;
	static string kierunkiPlik;
	static string trybyStudiowPlik;
	static string tytulyNaukowePlik;
	static string wojewodztwaPlik;
	static string miastaPlik;
	static string przedmiotyMaturalnePlik;

	//Odpowiedzi udzielone przez u�ytkownika
	int zainteresowanie = 0;
	int kierunek = 0;
	int trybStudiow = 0;
	int tytulNaukowy = 0;
	int wojewodztwoMieszka = 0;
	int czyWWojewodztwie = 0;
	int wojewodztwoUczelnia = 0;
	int miasto = 0;
	vector<int> wynikiZMatury;
	int ileRozszerzen = 0;
	vector<int> wynikRozszerzen;
	int czyOlimpiada = 0;

	/*
		Funkcja wczytuje odpowiedzi z wszystkich plik�w z odpowiedziami.
	*/
	void wczytajOdpowiedzi();

	/*
		Funkcja s�u��ca do zadania pytania u�ytkownikowi.

		Parametry
			pytanie - napis zawieraj�cy pyanie, jakie wy�wietli si� u�ytkownikowi
			odpowiedzi - wektor mo�liwych odpowiedzi

		Zwraca
			indeks odpowiedzi udzielonej przez u�ytkownika w wej�ciowym wektorze
	*/
	int zadajPytanieWPrzedziale(string pytanie, vector<string>& odpowiedzi);

	/*
		Funkcja s�u��ca do zadania pytania u�ytkownikowi, gdzie mo�liwe s� tylko
		odpowiedzi tak lub nie.

		Parametry
			pytanie - napis zawieraj�cy pyanie, jakie wy�wietli si� u�ytkownikowi

		Zwraca
			dowoln� liczb� != 0, je�li u�ytkownik odpowie tak, w przeciwnym przypadku 0.
	*/
	int zadajPytanieTakNie(string pytanie);

	/*
		Funkcja s�u��ca do zadania pytania u�ytkownikowi.

		Parametry
			pytanie - napis zawieraj�cy pyanie, jakie wy�wietli si� u�ytkownikowi
			min - minimaln� liczb� jak� mo�e wpisa�
			max - maksymaln� liczb� jak� mo�e wpisa�
		Zwraca
			wczytan� liczb�
	*/
	int wczytajLiczbe(string pytanie, int min = 0, int max = 100);

	/*
		Funkcja wczytuje mo�liwe do wyboru odpowiedzi z pliku tekstowego.
		Wczytane odpowiedzi zapisuje do wektora;

		Parametry:
			v - wektor string�w do kt�rego zapiszemy odpowiedzi
			plik - URL pliku z kt�rego czytamy odpowiedzi
	*/
	void wczytajOdpowiedzi(vector<string> &v, string& plik);

	/*
		Funkcja wczytuje mo�liwe do wyboru odpowiedzi z pliku tekstowego.
		Plik zawira odpowiedzi wraz z ich kategoriami.
		Do wczytanej kategorii i odpowiedzi do niej przypisanych tworzy
		obiekt pair, gdzie pierwszy parametr jest kategori�, a drugi parametr jest
		list� (wektorem string�w) odpowiedzi.

		Parametry:
			v - wektor par string�w i wektora string�w, gdzie string jest kategori�,
				a wektor string�w odpowiedziami w danej kategorii
			plik - URL pliku z kt�rego czytamy odpowiedzi
	*/
	void wczytajOdpowiedzi(vector<pair<string, vector<string>>> &v, string& plik);

public:
	//Getery
	string getZainteresowanie();
	string getKierunek();
	string getTrybStudiow();
	string getTytulNaukowy();
	string getWojewodztwoMieszka();
	bool getCzyWWojewodztwie();
	string getWojewodztwoUczelnia();
	string getMiasto();
	vector<int>& getWynikiZMatury();
	int getIleRozszerzen();
	vector<int>& getWynikRozszerzen();
	bool getCzyOlimpiada();

	/*
		Konstruktor klasy Odpowiedzi;
	*/
	Odpowiedzi();

	/*
		Funkcja zadaj�ca wszystkie pytania u�ytkownikowi.
	*/
	void pytaj();
};
