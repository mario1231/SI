#include <iostream>

#include "Odpowiedzi.h"
#include "Uczelnie.h"
#include "Poszukiwacz.h"

using namespace std;

int main() {
	//Tworzymy obiekty
	Odpowiedzi odpowiedzi;
	Uczelnie uczelnie;
	Poszukiwacz poszukiwacz(uczelnie, odpowiedzi);

	string tekst = "";

	//Wyświetlamy opis
	cout << "Witaj w systemie eksperckim\n\n blabla \n\n";

	do {
		//Zadajemy pytania
		odpowiedzi.pytaj();

		//Szukamy i wyświetlamy uczelnię.
		poszukiwacz.wyszukaj();

		cout << "\n\nJesli chcesz wyszukac ponownie wpisz p. Dowolny inny tekst spowoduje wyjscie z programu: ";
		cin >> tekst;
		cout << "\n\n\n\n";
	} while (tekst == "p");

	return 0;
}