#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <algorithm>
#include <limits>
#include <fstream>
#include "Odpowiedzi.h"

using namespace std;

string Odpowiedzi::folderPlikow = "pliki/";

string Odpowiedzi::zainteresowaniaPlik = "zainteresowania.txt";
string Odpowiedzi::kierunkiPlik = "kierunki.txt";
string Odpowiedzi::trybyStudiowPlik = "tryby_studiow.txt";
string Odpowiedzi::tytulyNaukowePlik = "tytuly_naukowe.txt";
string Odpowiedzi::wojewodztwaPlik = "wojewodztwa.txt";
string Odpowiedzi::miastaPlik = "miasta.txt";
string Odpowiedzi::przedmiotyMaturalnePlik = "przedmioty_maturalne.txt";

Odpowiedzi::Odpowiedzi() {
	wczytajOdpowiedzi();
}

//Getery 
string Odpowiedzi::getZainteresowanie() { return zainteresowania.at(zainteresowanie); }
string Odpowiedzi::getKierunek() { return kierunki.at(kierunek); }
string Odpowiedzi::getTrybStudiow() { return trybyStudiow.at(trybStudiow); }
string Odpowiedzi::getTytulNaukowy() { return tytulyNaukowe.at(tytulNaukowy); }
string Odpowiedzi::getWojewodztwoMieszka() { return wojewodztwa.at(wojewodztwoMieszka); }
bool Odpowiedzi::getCzyWWojewodztwie() { return czyWWojewodztwie; }
string Odpowiedzi::getWojewodztwoUczelnia() { return wojewodztwa.at(wojewodztwoMieszka); }
string Odpowiedzi::getMiasto() { return miasta.at(czyWWojewodztwie ? wojewodztwoMieszka : wojewodztwoUczelnia).second.at(miasto); }
vector<int>& Odpowiedzi::getWynikiZMatury() { return wynikiZMatury; }
int Odpowiedzi::getIleRozszerzen() { return ileRozszerzen; }
vector<int>& Odpowiedzi::getWynikRozszerzen() { return wynikRozszerzen; }
bool Odpowiedzi::getCzyOlimpiada() { return czyOlimpiada; }

void Odpowiedzi::wczytajOdpowiedzi(vector<string> &v, string& plik) {
	ifstream fOut;
	fOut.open(Odpowiedzi::folderPlikow + plik, ios::in);
	string buffer;

	if (fOut.good()) {
		while (!fOut.eof()) {
			buffer.clear();
			getline(fOut, buffer);
			v.push_back(buffer);
		}

		fOut.close();
	}
	else
		cout << "Blad otwarcia pliku " << plik << ".txt\n";
}

void Odpowiedzi::wczytajOdpowiedzi(vector<pair<string, vector<string>>> &v, string& plik) {
	ifstream fOut;
	fOut.open(Odpowiedzi::folderPlikow + plik, ios::in);
	string bufferFirst;
	string bufferSecond;

	if (fOut.good()) {
		while (!fOut.eof()) {
			bufferFirst.clear();
			bufferSecond.clear();

			getline(fOut, bufferFirst);
			getline(fOut, bufferSecond);

			pair<string, vector<string>> p;
			p.first = bufferFirst;

			char* m;
			char* bufferChar = new char[bufferSecond.length() + 1];
			strcpy(bufferChar, bufferSecond.c_str());
			m = strtok(bufferChar, ",");
			
			while (m != NULL) {
				p.second.push_back(string(m));
				m = strtok(NULL, ",");
			}

			delete []bufferChar;
			v.push_back(p);
		}

		fOut.close();
	}
	else
		cout << "Blad otwarcia pliku " << plik << ".txt\n";
}

void Odpowiedzi::wczytajOdpowiedzi() {
	wczytajOdpowiedzi(zainteresowania, zainteresowaniaPlik);
	wczytajOdpowiedzi(kierunki, kierunkiPlik);
	wczytajOdpowiedzi(trybyStudiow, trybyStudiowPlik);
	wczytajOdpowiedzi(tytulyNaukowe, tytulyNaukowePlik);
	wczytajOdpowiedzi(wojewodztwa, wojewodztwaPlik);
	wczytajOdpowiedzi(miasta, miastaPlik);
	wczytajOdpowiedzi(przedmiotyMaturalne, przedmiotyMaturalnePlik);
}

int Odpowiedzi::zadajPytanieWPrzedziale(string pytanie, vector<string> &v) {
	int w;

	cout << pytanie << "\n";
	for (int i = 0; i < v.size(); ++i) {
		if (i % 4 == 0)
			cout << endl;

		cout << (i + 1) << ") " << v.at(i) << "  ";
	}

	cout << "\nPodaj odpowiedz: ";

	while (!(cin >> w) || w < 1 || w > v.size()) {
		cout << "Niepoprawna odpowiedz! Podaj ponownie: ";
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}

	cout << "\n\n";

	return w-1;
}

int Odpowiedzi::zadajPytanieTakNie(string pytanie) {
	int w;
	cout << pytanie << "\n";
	cout << "1) Tak  2) Nie\n";

	cout << "\nPodaj odpowiedz: ";

	while (!(cin >> w) || w < 1 || w > 2) {
		cout << "Niepoprawna odpowiedz! Podaj ponownie: ";
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}

	cout << "\n\n";

	return (int)(!(w-1));
}

int Odpowiedzi::wczytajLiczbe(string pytanie, int min, int max) {
	int w;
	cout << pytanie << "\n";
	cout << "\nPodaj odpowiedz: ";

	while (!(cin >> w) || w < min || w > max) {
		cout << "Niepoprawna odpowiedz! Podaj ponownie: ";
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}

	return w;
}

void Odpowiedzi::pytaj() {
	zainteresowanie = zadajPytanieWPrzedziale("1.1 Co Cie interesuje?", zainteresowania);
	kierunek = zadajPytanieWPrzedziale("1.2 Jaki kierunek Cie interesuje?", kierunki);
	trybStudiow = zadajPytanieWPrzedziale("1.3 Jaki tryb studiow Cie interesuje?", trybyStudiow);
	tytulNaukowy = zadajPytanieWPrzedziale("1.4 Jaki chcesz zdobyc tytul naukowy?", tytulyNaukowe);

	wojewodztwoMieszka = zadajPytanieWPrzedziale("2.1 W jakim wojewodztwie mieszkasz?", wojewodztwa);
	czyWWojewodztwie = zadajPytanieTakNie("2.2a Czy uczelnia ma byc tylko w obrebie wojewodztwa, w ktorym mieszkasz?");

	if (!czyWWojewodztwie) 
		wojewodztwoUczelnia = zadajPytanieWPrzedziale("2.2b W jakim wojewodztwie chcesz znalezc uczelnie?", wojewodztwa);

	miasto = zadajPytanieWPrzedziale("2.4 W jakim miescie z podanego wojewodztwa, chcesz studiowac?",
		[](vector<pair<string, vector<string>>>& miasta, int wojewodztwo) -> vector<string> {
		return miasta.at(wojewodztwo).second;
	}(miasta, (czyWWojewodztwie ? wojewodztwoMieszka : wojewodztwoUczelnia)));


	cout << "3.1 Podaj procent punktow uzyskanych na maturze z nastepujących przedmiotow: \n";
	wynikiZMatury.resize(3);
	wynikiZMatury[0] = wczytajLiczbe("Jezyk polski", 30);
	wynikiZMatury[1] = wczytajLiczbe("Matematyka", 30);
	wynikiZMatury[2] = wczytajLiczbe("Jezyk obcy", 30);

	ileRozszerzen = wczytajLiczbe("3.2 Podaj liczbe rozszerzen zdawanych na maturze");

	cout << "3.3 Podaj wynik procentowy z przedmiotu rozszerzonego:\n";
	for (int i = 0; i < ileRozszerzen; ++i)
		wynikRozszerzen.push_back(wczytajLiczbe("Przedmiot nr " + to_string(i + 1)));

	czyOlimpiada = zadajPytanieTakNie("3.4 Czy brales/as udzial w olimpiadzie naukowej?");
}