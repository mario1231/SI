#pragma once

#include <vector>
#include "Uczelnia.h"

/*
	Klasa Uczelnie

	Reprezentuje zbi�r uczelni wczytanych z pliku tekstowego.
*/
class Uczelnie {
private:
	//Wektor uczelni.
	vector<Uczelnia> uczelnie;
	//URL pliku tekstowego zawieraj�cy spis uczelni.
	static string uczelniePlik;

	/*
		Funkcja wczytuje zbi�r uczelni z pliku tekstowego.
	*/
	void wczytajUczelnie();

public:
	/*
		Konstruktor klasy Uczelnie;
	*/
	Uczelnie();

	/*
		Getter zwracaj�cy list� wszystkich uczelni.

		Zwraca
			wektor uczelni
	*/
	vector<Uczelnia> getUczelnie();

	/*
		Funkcja sortuj�ca wektor uczelni na podstawie miejsca w rankingu.
	*/
	void sortujPoRankingu();
};