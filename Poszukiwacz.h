#pragma once

#include "Uczelnie.h"
#include "Odpowiedzi.h"

/*
	Klasa Poszukiwacz

	Jej zadaniem jest znalezienie odpowiedniej uczelni na podstawie odpowiedzi
	udzielonych przez użytkownika.
*/
class Poszukiwacz {
private:
	//Referencja do obiektu reprezentującego uczelnie
	Uczelnie& uczelnie;
	//Referencja do obiektu reprezentującego udzielone odpowiedzi
	Odpowiedzi& odpowiedzi;
public:
	/*
		Konstruktor klasy Poszukiwacz

		Parametry
			u - referencja do obiektu reprezentującego uczelnie
			o - referencja do obiektu reprezentującego odpowiedzi
	*/
	Poszukiwacz(Uczelnie& u, Odpowiedzi& o);

	/*
		Funkcja wyszukująca uczelnię na podstawie udzielonych przez użytkownika odpowiedzi.
		Jeśli znajdzie szukaną uczelnię - wyświetla jej nazwę. Jeśli nie - wyświetla stosowny komunikat.
	*/
	void wyszukaj();
};